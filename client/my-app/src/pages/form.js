import React, { useState } from "react"
import axios from "axios";
import CustomTable from "../components/table";
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';

const useStyles = makeStyles((theme) => ({
  root: {
    height: 300,
    flexGrow: 1,
    minWidth: 300,
    transform: 'translateZ(0)',
    // The position fixed scoping doesn't work in IE 11.
    // Disable this demo to preserve the others.
    '@media all and (-ms-high-contrast: none)': {
      display: 'none',
    },
  },
  modal: {
    display: 'flex',
    padding: theme.spacing(1),
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

const columns = [
  { id: 'name', label: 'Name', minWidth: 170 },
  { id: 'phonenumber', label: 'PHONE\u00a0NUMBER', minWidth: 100 },
  {
    id: 'message',
    label: 'MESSAGE',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toLocaleString('en-US'),
    },
  {
    id: 'email',
    label: 'EMAIL',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toLocaleString('en-US'),
  },
    {
    id: 'update',
    label: "UPDATE",
    minWidth: 170,
    align: 'right',
    format: (value) => value.toLocaleString('en-US'),
    },
     {
    id: 'delete',
    label: 'DELETE',
    minWidth: 170,
    align: 'right',
    format: (value) => value.toLocaleString('en-US'),
  },
  
];



const Form = props => {
     const classes = useStyles();
  const rootRef = React.useRef(null);

    const [addDiv, setAddDiv] = useState(true)
    const [viewDiv, setView] = useState(false);
    const [viewData ,setViewData] = useState([])
    const [name, setName] = useState(null);
    const [phonenumber, setphonenumber] = useState(null);
    const [message, setmessage] = useState(null);
    const [email, setmail] = useState(null);
    const [updateId, setId] = useState([]);
     const [showModal, setModal] = useState(false);

    const submitForm = () => {
        const data = {
            name: name,
            phonenumber: phonenumber,
            message: message,
            email:email
        }
        setName("");
        setphonenumber("");
        setmessage("");
        setmail("")

  
        axios.post("http://localhost:4000/api/insert", data).then((res) => {
            console.log(res)
           alert("Inserted !!")
      
        }).catch((err) => {
            console.log(err)
        })
       
    }
    const udpateMethod = (row) => {
        setId(row)
        setModal(true)
    }

    const closeIt = (e) => {
        setModal(false)
    }
    const deleteData = (row) => {
      console.log(row)
      axios.delete(`http://localhost:4000/api/delete/${row}`).then((res) => {
            console.log(res)
        
      
        }).catch((err) => {
            console.log(err)
        })
        setTimeout(() => {
            alert("Deleted !!")
            window.location.reload()
        }, 1000);
}
    const updateIt = (e) => {
          axios.put("http://localhost:4000/api/update", updateId).then((res) => {
              console.log(res)
            
        }).catch((err) => {
            console.log(err)
        })
        setTimeout(() => {
             setModal(false)
             alert("Updated !!")
            window.location.reload()
        }, 1000);
    }

    return (<div>
        {addDiv && <center>
            <h1>ADD</h1>
            <div>
                <h3>Name:</h3>
                <input type="text" placeHolder="enter name" value={name} onChange={(e) => {
                    setName(e.target.value)
                }}></input>
            </div>
            <div>
                <h3>PNo:</h3>
                <input type="text" placeHolder="enter PNo" value={phonenumber} onChange={(e) => {
                    setphonenumber(e.target.value)
                }}></input>
            </div>
            <div>
                <h3>Msg:</h3>
                <input type="text" placeHolder="enter Msg" value={message} onChange={(e) => {
                    setmessage(e.target.value)
                }}></input>
            </div>
            <div>
                <h3>Email:</h3>
                <input type="text" placeHolder="enter Email" value={email} onChange={(e) => {
                    setmail(e.target.value)
                }}></input>
            </div>
            <button onClick={submitForm}>Submit</button>
               <div>
                <button onClick={(e) => {
                    try {
                        setAddDiv(false);
                        setView(true);
                        axios.get("http://localhost:4000/api/view").then((res) => {
                           setViewData(res.data)
                        }).catch((err)=>{console.log(err)})
                    }catch (err) { }
            }}>
                
                View Data
            </button>
        </div>
        </center>}
        {
            viewDiv && <center>

                <CustomTable columns={columns} storeApi={viewData} title="VIEW" udpateMethod={udpateMethod} deleteData={deleteData}/>
                     
                 <div>
                <button onClick={(e) => {
                    try {
                        setAddDiv(true);
                        setView(false);
                       
                    }catch (err) { }
            }}>
                
                ADD  DATA
            </button>
        </div>
            </center>
        }
        {showModal && <Modal
  disablePortal
  disableEnforceFocus
  disableAutoFocus
  open
  aria-labelledby="server-modal-title"
  aria-describedby="server-modal-description"
            className={classes.modal}
            
  container={() => rootRef.current}
>
            <div className={classes.paper}>
                <center>
    <h2 id="server-modal-title">UPDATE DATA</h2>
                    <input type="text" value={updateId.name} placeholder="Enter name" onChange={(e) => {
                      
                            setId(prevState => {
    return { ...prevState, name: e.target.value }
  });
                     
                    }}></input>
                    <input type="number" value={updateId.phonenumber} placeholder="Enter phoneNumber"
                    onChange={(e) => {
                        setId(n => {
                        
                            return {
                                ...n,phonenumber:e.target.value
                            }
                        })
                    }}></input>
                    <input type="text" value={updateId.message} placeholder="Enter message" onChange={(e) => {
                        setId(n => {
                            return {
                                ...n,message:e.target.value
                            }
                        })
                    }}></input>
                     <input type="email" value={updateId.email} placeholder="Enter email" onChange={(e) => {
                        setId(n => {
                            return {
                              ...n, email:e.target.value
                          }
                        })
                    }}></input>
                    <button onClick={(e) => { updateIt(e) }}>Update</button>
                    <button onClick={(e) => { closeIt(e) }}>close</button>
                    </center>
  </div>
</Modal>}
     
    </div>)
}

export default Form